package com.brainpowered.courtcounter;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {
    int teamAScore = 0;
    int teamBScore = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void freeThrowA(View view) {
        teamAScore += 1;
        displayTeamAScore(teamAScore);
    }

    public void plusTwoA(View view) {
        teamAScore += 2;
        displayTeamAScore(teamAScore);
    }

    public void plusThreeA(View view) {
        teamAScore += 3;
        displayTeamAScore(teamAScore);
    }

    public void freeThrowB(View view) {
        teamBScore += 1;
        displayTeamBScore(teamBScore);
    }

    public void plusTwoB(View view) {
        teamBScore += 2;
        displayTeamBScore(teamBScore);
    }

    public void plusThreeB(View view) {
        teamBScore += 3;
        displayTeamBScore(teamBScore);
    }

    public void displayTeamAScore(int number){
        TextView teamAScore = (TextView)findViewById(R.id.team_a_score);
        teamAScore.setText("" + number);
    }

    public void displayTeamBScore(int number){
        TextView teamBScore = (TextView)findViewById(R.id.team_b_score);
        teamBScore.setText(""+number);
    }

    public void reset(View view) {
        teamAScore = 0;
        teamBScore = 0;
        displayTeamAScore(teamAScore);
        displayTeamBScore(teamBScore);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
